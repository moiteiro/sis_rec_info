@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h2 id="typography">Search</h2>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <form method="get" action="/">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input class="form-control" type="text" name="search" placeholder="Enter your search"
                            @if (isset($input['search']))
                                value="{{$input['search']}}"
                            @endif
                        >
                    </div>
                    <div class="form-group col-md-4">
                        <select id="searchType" name="searchType" class="form-control">
                            <option value="1" 
                                @if ($input['searchType'] == 1)
                                selected="selected"
                                @endif
                            >boolean</option>
                            <option value="2"
                                @if ($input['searchType'] == 2)
                                selected="selected"
                                @endif
                            >vectorial</option>
                    </select>
                    </div>
                    <div class="form-group col-md-2">
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </span>
                    </div>
                    @if (isset($statistics) && $statistics['total'] > 0)
                        <p class="help-block text-center">Total of scenes: {{$statistics['total']}}</p>
                    @elseif (isset($statistics) && $statistics['total'] == 0)
                        <p class="help-block text-center">No scenes for this search</p>
                    @endif
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @foreach($documents as $document)
                <h3>{{$document->scene}}</h3>
                <p style="white-space: pre-wrap;">
                {{$document->content}}
                </p>
                <hr style="border-top: 2px solid black;">
            @endforeach
        </div>
    </div>
</div>

@endsection