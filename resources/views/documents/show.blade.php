@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h2 id="typography">Document</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                
                <div class="panel-body">
                    <p style="white-space: pre-wrap;">{{$document->content}}</p>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection