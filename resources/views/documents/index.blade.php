@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h2 id="typography">Documents</h2>
        </div>
      </div>
    </div>

    <div class="row">
        <div class='col-sm-12'>
            <p class="bs-component">
                <a href="/documents/create" class="btn btn-primary">Create Document</a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> 
            <table class="table table-striped" data-form="deleteForm">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($documents as $document)
                        <tr>
                            <th scope="row" width="300">{{ $document->id }}</th>
                            <td>{{truncate($document->content, 400)}}</td>
                            <td width="140">
                                <a href='/documents/{{$document->id}}' class='btn btn-primary btn-xs'>View</a>
                                <a href='/documents/{{$document->id}}/edit' class='btn btn-primary btn-xs'>Edit</a>
                                <a href="#modal-dialog" class="remove-entry-button btn btn-xs btn-danger" data-toggle="modal" data-remove-url="documents/{{$document->id}}" data-entry-name="{{$document->title}}" data-entry-id="{{$document->id}}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>You are about to delete this record:</p>
                <p id="target_form_delete" ></p>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                {{ Form::open(['method' => 'DELETE', 'id' =>'remove_entry_confirmation_form', 'url' => '']) }}
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="close-btn">Close</button>
                    {{ Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection