<div class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">S.R.I</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">Search</a>
                </li>

                <li>
                    <a href="/documents">Manage Documents</a>
                </li>
            </ul>
        </div>
    </div>
</div>