## Sobre o trabalho

Resitorio:
https://bitbucket.org/moiteiro/sis_rec_info

video explicativo (5min):
https://youtu.be/ylNrDwXs2tc

Endereco online:
http://159.89.230.119

Os arquivos de interesse deste trabalho estao localizados em:
- app/Models/Document.php
- app/Http/Controllers/SearchController.php

Ato escolhido para o trabalho:
[link](http://www.opensourceshakespeare.org/views/plays/play_view.php?WorkID=henry4p1&Act=5&Scope=act&pleasewait=1&msg=pl)