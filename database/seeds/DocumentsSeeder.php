<?php

use App\Models\Document as Document;
use Illuminate\Database\Seeder;

class DocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Document::create([
'content' => "[Enter KING HENRY, PRINCE HENRY, Lord John of LANCASTER, EARL OF WESTMORELAND, SIR WALTER BLUNT, and FALSTAFF]

Henry IV. How bloodily the sun begins to peer 
Above yon busky hill! the day looks pale 
At his distemperature.

Henry V. The southern wind 
Doth play the trumpet to his purposes, 
And by his hollow whistling in the leaves 
Foretells a tempest and a blustering day.

Henry IV. Then with the losers let it sympathize, 
For nothing can seem foul to those that win. 
[The trumpet sounds] 
[Enter WORCESTER and VERNON] 
How now, my Lord of Worcester! 'tis not well 
That you and I should meet upon such terms 
As now we meet. You have deceived our trust, 
And made us doff our easy robes of peace, 
To crush our old limbs in ungentle steel: 
This is not well, my lord, this is not well. 
What say you to it? will you again unknit 
This curlish knot of all-abhorred war? 
And move in that obedient orb again 
Where you did give a fair and natural light, 
And be no more an exhaled meteor, 
A prodigy of fear and a portent 
Of broached mischief to the unborn times?

Earl of Worcester. Hear me, my liege:
For mine own part, I could be well content 
To entertain the lag-end of my life 
With quiet hours; for I do protest, 
I have not sought the day of this dislike.


Henry IV. You have not sought it! how comes it, then?

Falstaff. Rebellion lay in his way, and he found it.

Henry V. Peace, chewet, peace!

Earl of Worcester. It pleased your majesty to turn your looks 
Of favour from myself and all our house; 
And yet I must remember you, my lord,
We were the first and dearest of your friends. 
For you my staff of office did I break 
In Richard's time; and posted day and night 
to meet you on the way, and kiss your hand, 
When yet you were in place and in account
Nothing so strong and fortunate as I. 
It was myself, my brother and his son, 
That brought you home and boldly did outdare 
The dangers of the time. You swore to us, 
And you did swear that oath at Doncaster,
That you did nothing purpose 'gainst the state; 
Nor claim no further than your new-fall'n right, 
The seat of Gaunt, dukedom of Lancaster: 
To this we swore our aid. But in short space 
It rain'd down fortune showering on your head;
And such a flood of greatness fell on you, 
What with our help, what with the absent king, 
What with the injuries of a wanton time, 
The seeming sufferances that you had borne, 
And the contrarious winds that held the king
So long in his unlucky Irish wars 
That all in England did repute him dead: 
And from this swarm of fair advantages 
You took occasion to be quickly woo'd 
To gripe the general sway into your hand;
Forget your oath to us at Doncaster; 
And being fed by us you used us so 
As that ungentle hull, the cuckoo's bird, 
Useth the sparrow; did oppress our nest; 
Grew by our feeding to so great a bulk
That even our love durst not come near your sight 
For fear of swallowing; but with nimble wing 
We were enforced, for safety sake, to fly 
Out of sight and raise this present head; 
Whereby we stand opposed by such means
As you yourself have forged against yourself 
By unkind usage, dangerous countenance, 
And violation of all faith and troth 
Sworn to us in your younger enterprise.

Henry IV. These things indeed you have articulate, 
Proclaim'd at market-crosses, read in churches, 
To face the garment of rebellion 
With some fine colour that may please the eye 
Of fickle changelings and poor discontents, 
Which gape and rub the elbow at the news 
Of hurlyburly innovation: 
And never yet did insurrection want 
Such water-colours to impaint his cause; 
Nor moody beggars, starving for a time 
Of pellmell havoc and confusion.

Henry V. In both your armies there is many a soul 
Shall pay full dearly for this encounter, 
If once they join in trial. Tell your nephew, 
The Prince of Wales doth join with all the world 
In praise of Henry Percy: by my hopes,
This present enterprise set off his head, 
I do not think a braver gentleman, 
More active-valiant or more valiant-young, 
More daring or more bold, is now alive 
To grace this latter age with noble deeds.
For my part, I may speak it to my shame, 
I have a truant been to chivalry; 
And so I hear he doth account me too; 
Yet this before my father's majesty— 
I am content that he shall take the odds
Of his great name and estimation, 
And will, to save the blood on either side, 
Try fortune with him in a single fight.

Henry IV. And, Prince of Wales, so dare we venture thee, 
Albeit considerations infinite
Do make against it. No, good Worcester, no, 
We love our people well; even those we love 
That are misled upon your cousin's part; 
And, will they take the offer of our grace, 
Both he and they and you, every man
Shall be my friend again and I'll be his: 
So tell your cousin, and bring me word 
What he will do: but if he will not yield, 
Rebuke and dread correction wait on us 
And they shall do their office. So, be gone;
We will not now be troubled with reply: 
We offer fair; take it advisedly.

[Exeunt WORCESTER and VERNON]

Henry V. It will not be accepted, on my life: 
The Douglas and the Hotspur both together
Are confident against the world in arms.

Henry IV. Hence, therefore, every leader to his charge; 
For, on their answer, will we set on them: 
And God befriend us, as our cause is just!

[Exeunt all but PRINCE HENRY and FALSTAFF]

Falstaff. Hal, if thou see me down in the battle and bestride 
me, so; 'tis a point of friendship.

Henry V. Nothing but a colossus can do thee that friendship. 
Say thy prayers, and farewell.

Falstaff. I would 'twere bed-time, Hal, and all well.

Henry V. Why, thou owest God a death.

[Exit PRINCE HENRY]

Falstaff. 'Tis not due yet; I would be loath to pay him before 
his day. What need I be so forward with him that 
calls not on me? Well, 'tis no matter; honour pricks
me on. Yea, but how if honour prick me off when I 
come on? how then? Can honour set to a leg? no: or 
an arm? no: or take away the grief of a wound? no. 
Honour hath no skill in surgery, then? no. What is 
honour? a word. What is in that word honour? what
is that honour? air. A trim reckoning! Who hath it? 
he that died o' Wednesday. Doth he feel it? no. 
Doth he hear it? no. 'Tis insensible, then. Yea, 
to the dead. But will it not live with the living? 
no. Why? detraction will not suffer it. Therefore
I'll none of it. Honour is a mere scutcheon: and so 
ends my catechism.

[Exit]
", 
"words" => " enter king henry prince lord john lancaster earl westmoreland sir walter blunt falstaff iv bloodily sun begins peer yon busky hill day looks pale distemperature southern wind doth play trumpet purposes hollow whistling leaves foretells tempest blustering losers sympathize foul win sounds worcester vernon tis meet terms deceived trust doff easy robes peace crush limbs ungentle steel unknit curlish knot all-abhorred war move obedient orb fair natural light exhaled meteor prodigy fear portent broached mischief unborn times hear liege mine own content entertain lag-end life quiet hours protest sought dislike comes rebellion lay found chewet pleased majesty favour house remember dearest friends staff office break richard time posted night kiss hand account strong fortunate brother son brought home boldly outdare dangers swore swear oath doncaster purpose gainst nor claim new-fall seat gaunt dukedom aid short space rain fortune showering head flood greatness fell help absent injuries wanton sufferances borne contrarious winds held unlucky irish wars england repute dead swarm advantages occasion quickly woo gripe sway forget fed hull cuckoo bird useth sparrow oppress nest grew feeding bulk love durst near sight swallowing nimble wing enforced safety sake fly raise whereby stand opposed means yourself forged unkind usage dangerous countenance violation faith troth sworn enterprise indeed articulate proclaim market-crosses read churches garment fine colour please eye fickle changelings poor discontents gape rub elbow news hurlyburly innovation insurrection water-colours impaint cause moody beggars starving pellmell havoc confusion armies soul pay dearly encounter join trial tell nephew wales world praise percy hopes set braver gentleman active-valiant valiant-young daring bold alive grace latter age noble deeds speak shame truant chivalry father majesty— odds name estimation save blood try single fight dare venture thee albeit considerations infinite people misled cousin offer friend ll bring word yield rebuke dread correction wait gone troubled reply advisedly exeunt accepted douglas hotspur confident arms hence leader charge answer god befriend hal thou battle bestride friendship colossus thy prayers farewell twere bed-time owest death exit due loath forward calls matter honour pricks yea prick leg arm grief wound hath skill surgery air trim reckoning died wednesday feel insensible live living detraction suffer none mere scutcheon catechism "
		]);



	###################### SCENE 2 ########################
	Document::create([
'content' => " [Enter WORCESTER and VERNON]

Earl of Worcester. O, no, my nephew must not know, Sir Richard,
The liberal and kind offer of the king.

Vernon. 'Twere best he did.

Earl of Worcester. Then are we all undone. 
It is not possible, it cannot be, 
The king should keep his word in loving us;
He will suspect us still and find a time 
To punish this offence in other faults: 
Suspicion all our lives shall be stuck full of eyes; 
For treason is but trusted like the fox, 
Who, ne'er so tame, so cherish'd and lock'd up,
Will have a wild trick of his ancestors. 
Look how we can, or sad or merrily, 
Interpretation will misquote our looks, 
And we shall feed like oxen at a stall, 
The better cherish'd, still the nearer death.
My nephew's trespass may be well forgot; 
it hath the excuse of youth and heat of blood, 
And an adopted name of privilege, 
A hair-brain'd Hotspur, govern'd by a spleen: 
All his offences live upon my head
And on his father's; we did train him on, 
And, his corruption being ta'en from us, 
We, as the spring of all, shall pay for all. 
Therefore, good cousin, let not Harry know, 
In any case, the offer of the king.

Vernon. Deliver what you will; I'll say 'tis so. 
Here comes your cousin.

[Enter HOTSPUR and DOUGLAS]

Hotspur (Henry Percy). My uncle is return'd: 
Deliver up my Lord of Westmoreland.
Uncle, what news?

Earl of Worcester. The king will bid you battle presently.

Earl of Douglas. Defy him by the Lord of Westmoreland.

Hotspur (Henry Percy). Lord Douglas, go you and tell him so.

Earl of Douglas. Marry, and shall, and very willingly.

[Exit]

Earl of Worcester. There is no seeming mercy in the king.

Hotspur (Henry Percy). Did you beg any? God forbid!

Earl of Worcester. I told him gently of our grievances, 
Of his oath-breaking; which he mended thus,
By now forswearing that he is forsworn: 
He calls us rebels, traitors; and will scourge 
With haughty arms this hateful name in us.

[Re-enter the EARL OF DOUGLAS]

Earl of Douglas. Arm, gentlemen; to arms! for I have thrown
A brave defiance in King Henry's teeth, 
And Westmoreland, that was engaged, did bear it; 
Which cannot choose but bring him quickly on.

Earl of Worcester. The Prince of Wales stepp'd forth before the king, 
And, nephew, challenged you to single fight.

Hotspur (Henry Percy). O, would the quarrel lay upon our heads, 
And that no man might draw short breath today 
But I and Harry Monmouth! Tell me, tell me, 
How show'd his tasking? seem'd it in contempt?

Vernon. No, by my soul; I never in my life
Did hear a challenge urged more modestly, 
Unless a brother should a brother dare 
To gentle exercise and proof of arms. 
He gave you all the duties of a man; 
Trimm'd up your praises with a princely tongue,
Spoke to your deservings like a chronicle, 
Making you ever better than his praise 
By still dispraising praise valued in you; 
And, which became him like a prince indeed, 
He made a blushing cital of himself;
And chid his truant youth with such a grace 
As if he master'd there a double spirit. 
Of teaching and of learning instantly. 
There did he pause: but let me tell the world, 
If he outlive the envy of this day,
England did never owe so sweet a hope, 
So much misconstrued in his wantonness.

Hotspur (Henry Percy). Cousin, I think thou art enamoured 
On his follies: never did I hear 
Of any prince so wild a libertine. 
But be he as he will, yet once ere night 
I will embrace him with a soldier's arm, 
That he shall shrink under my courtesy. 
Arm, arm with speed: and, fellows, soldiers, friends, 
Better consider what you have to do 
Than I, that have not well the gift of tongue, 
Can lift your blood up with persuasion.

[Enter a Messenger]

Messenger. My lord, here are letters for you.

Hotspur (Henry Percy). I cannot read them now.
O gentlemen, the time of life is short! 
To spend that shortness basely were too long, 
If life did ride upon a dial's point, 
Still ending at the arrival of an hour. 
An if we live, we live to tread on kings;
If die, brave death, when princes die with us! 
Now, for our consciences, the arms are fair, 
When the intent of bearing them is just.

[Enter another Messenger]

Messenger. My lord, prepare; the king comes on apace.

Hotspur (Henry Percy). I thank him, that he cuts me from my tale, 
For I profess not talking; only this— 
Let each man do his best: and here draw I 
A sword, whose temper I intend to stain 
With the best blood that I can meet withal 
In the adventure of this perilous day. 
Now, Esperance! Percy! and set on. 
Sound all the lofty instruments of war, 
And by that music let us all embrace; 
For, heaven to earth, some of us never shall 
A second time do such a courtesy.

[The trumpets sound. They embrace, and exeunt]
", 'words' => ' [enter worcester vernon] earl nephew sir richard liberal offer king vernon twere undone word loving suspect time punish offence faults suspicion lives stuck eyes treason trusted fox ne er tame cherish lock wild trick ancestors look sad merrily interpretation misquote looks feed oxen stall nearer death trespass forgot hath excuse youth heat blood adopted name privilege hair-brain hotspur govern spleen offences live head father train corruption ta en spring pay cousin harry deliver ll tis comes douglas] (henry percy) uncle return lord westmoreland news bid battle presently douglas defy tell marry willingly [exit] mercy beg god forbid told gently grievances oath-breaking mended forswearing forsworn calls rebels traitors scourge haughty arms hateful [re-enter arm gentlemen thrown brave defiance henry teeth engaged bear choose bring quickly prince wales stepp forth challenged single fight quarrel lay heads draw short breath monmouth tasking contempt soul life hear challenge urged modestly unless brother dare gentle exercise proof duties trimm praises princely tongue spoke deservings chronicle praise dispraising valued indeed blushing cital chid truant grace master double spirit teaching learning instantly pause world outlive envy day england owe sweet hope misconstrued wantonness thou art enamoured follies libertine ere night embrace soldier shrink courtesy speed fellows soldiers friends consider gift lift persuasion messenger] messenger letters read spend shortness basely ride dial arrival hour tread kings die princes consciences fair intent bearing prepare apace thank cuts tale profess talking this— sword temper intend stain meet withal adventure perilous esperance percy set sound lofty instruments war music heaven earth [the trumpets exeunt] ']);



	###################### SCENE 3 ########################
	Document::create([
'content' => " [KING HENRY enters with his power. Alarum to the battle. Then enter DOUGLAS and SIR WALTER BLUNT]

Blunt. What is thy name, that in the battle thus 
Thou crossest me? what honour dost thou seek
Upon my head?

Earl of Douglas. Know then, my name is Douglas; 
And I do haunt thee in the battle thus 
Because some tell me that thou art a king.

Blunt. They tell thee true.

Earl of Douglas. The Lord of Stafford dear to-day hath bought 
Thy likeness, for instead of thee, King Harry, 
This sword hath ended him: so shall it thee, 
Unless thou yield thee as my prisoner.

Blunt. I was not born a yielder, thou proud Scot; 
And thou shalt find a king that will revenge 
Lord Stafford's death.

[They fight. DOUGLAS kills SIR WALTER BLUNT.

Enter HOTSPUR]

Hotspur (Henry Percy). O Douglas, hadst thou fought at Holmedon thus,
never had triumph'd upon a Scot.

Earl of Douglas. All's done, all's won; here breathless lies the king.

Hotspur (Henry Percy). Where?

Earl of Douglas. Here.

Hotspur (Henry Percy). This, Douglas? no: I know this face full well:
A gallant knight he was, his name was Blunt; 
Semblably furnish'd like the king himself.

Earl of Douglas. A fool go with thy soul, whither it goes! 
A borrow'd title hast thou bought too dear: 
Why didst thou tell me that thou wert a king?

Hotspur (Henry Percy). The king hath many marching in his coats.

Earl of Douglas. Now, by my sword, I will kill all his coats; 
I'll murder all his wardrobe, piece by piece, 
Until I meet the king.

Hotspur (Henry Percy). Up, and away!
Our soldiers stand full fairly for the day.

[Exeunt]

[Alarum. Enter FALSTAFF, solus]

Falstaff. Though I could 'scape shot-free at London, I fear 
the shot here; here's no scoring but upon the pate.
Soft! who are you? Sir Walter Blunt: there's honour 
for you! here's no vanity! I am as hot as moulten 
lead, and as heavy too: God keep lead out of me! I 
need no more weight than mine own bowels. I have 
led my ragamuffins where they are peppered: there's
not three of my hundred and fifty left alive; and 
they are for the town's end, to beg during life. 
But who comes here?

[Enter PRINCE HENRY]

Henry V. What, stand'st thou idle here? lend me thy sword:
Many a nobleman lies stark and stiff 
Under the hoofs of vaunting enemies, 
Whose deaths are yet unrevenged: I prithee, 
lend me thy sword.

Falstaff. O Hal, I prithee, give me leave to breathe awhile.
Turk Gregory never did such deeds in arms as I have 
done this day. I have paid Percy, I have made him sure.

Henry V. He is, indeed; and living to kill thee. I prithee, 
lend me thy sword.

Falstaff. Nay, before God, Hal, if Percy be alive, thou get'st
not my sword; but take my pistol, if thou wilt.
Henry V. Give it to me: what, is it in the case?

Falstaff. Ay, Hal; 'tis hot, 'tis hot; there's that will sack a city.

[PRINCE HENRY draws it out, and finds it to be a bottle of sack]

Henry V. What, is it a time to jest and dally now?

[He throws the bottle at him. Exit]

Falstaff. Well, if Percy be alive, I'll pierce him. If he do 
come in my way, so: if he do not, if I come in his 
willingly, let him make a carbonado of me. I like 
not such grinning honour as Sir Walter hath: give me
life: which if I can save, so; if not, honour comes 
unlooked for, and there's an end.

[Exit FALSTAFF]
", 'words' => ' [king henry enters power alarum battle enter douglas sir walter blunt] blunt thy name thou crossest honour dost seek head earl haunt thee tell art king true lord stafford dear to-day hath bought likeness instead harry sword unless yield prisoner born yielder proud scot shalt revenge death [they fight kills hotspur] hotspur (henry percy) hadst fought holmedon triumph won breathless lies gallant knight semblably furnish fool soul whither goes borrow title hast didst wert marching coats kill ll murder wardrobe piece meet soldiers stand fairly day [exeunt] [alarum falstaff solus] scape shot-free london fear shot scoring pate soft vanity hot moulten lead heavy god weight mine own bowels led ragamuffins peppered hundred fifty left alive town beg life comes [enter prince henry] st idle lend nobleman stark stiff hoofs vaunting enemies deaths unrevenged prithee hal leave breathe awhile turk gregory deeds arms paid percy indeed living nay pistol wilt ay tis sack city [prince draws bottle sack] time jest dally [he throws exit] pierce willingly carbonado grinning save unlooked [exit falstaff] ']);



	###################### SCENE 4 ########################
	Document::create([
'content' => " [Alarum. Excursions. Enter PRINCE HENRY, LORD JOHN OF LANCASTER, and EARL OF WESTMORELAND]

Henry IV. I prithee,
Harry, withdraw thyself; thou bleed'st too much. 
Lord John of Lancaster, go you with him.

Prince John. Not I, my lord, unless I did bleed too.

Henry V. I beseech your majesty, make up, 
Lest your retirement do amaze your friends.

Henry IV. I will do so. 
My Lord of Westmoreland, lead him to his tent.

Earl of Westmoreland. Come, my lord, I'll lead you to your tent.

Henry V. Lead me, my lord? I do not need your help: 
And God forbid a shallow scratch should drive
The Prince of Wales from such a field as this, 
Where stain'd nobility lies trodden on, 
and rebels' arms triumph in massacres!

Prince John. We breathe too long: come, cousin Westmoreland, 
Our duty this way lies; for God's sake come.

[Exeunt LANCASTER and WESTMORELAND]

Henry V. By God, thou hast deceived me, Lancaster; 
I did not think thee lord of such a spirit: 
Before, I loved thee as a brother, John; 
But now, I do respect thee as my soul.

Henry IV. I saw him hold Lord Percy at the point 
With lustier maintenance than I did look for 
Of such an ungrown warrior.

Henry V. O, this boy 
Lends mettle to us all!

[Exit]


[Enter DOUGLAS]

Earl of Douglas. Another king! they grow like Hydra's heads: 
I am the Douglas, fatal to all those 
That wear those colours on them: what art thou,
That counterfeit'st the person of a king?

Henry IV. The king himself; who, Douglas, grieves at heart 
So many of his shadows thou hast met 
And not the very king. I have two boys 
Seek Percy and thyself about the field:
But, seeing thou fall'st on me so luckily, 
I will assay thee: so, defend thyself.

Earl of Douglas. I fear thou art another counterfeit; 
And yet, in faith, thou bear'st thee like a king: 
But mine I am sure thou art, whoe'er thou be,
And thus I win thee.

[They fight. KING HENRY being in danger, PRINCE HENRY enters]

Henry V. Hold up thy head, vile Scot, or thou art like 
Never to hold it up again! the spirits 
Of valiant Shirley, Stafford, Blunt, are in my arms:
It is the Prince of Wales that threatens thee; 
Who never promiseth but he means to pay. 
[They fight: DOUGLAS flies] 
Cheerly, my lord. how fares your grace? 
Sir Nicholas Gawsey hath for succor sent,
And so hath Clifton: I'll to Clifton straight.

Henry IV. Stay, and breathe awhile: 
Thou hast redeem'd thy lost opinion, 
And show'd thou makest some tender of my life, 
In this fair rescue thou hast brought to me.

Henry V. O God! they did me too much injury 
That ever said I hearken'd for your death. 
If it were so, I might have let alone 
The insulting hand of Douglas over you, 
Which would have been as speedy in your end
As all the poisonous potions in the world 
And saved the treacherous labour of your son.

Henry IV. Make up to Clifton: I'll to Sir Nicholas Gawsey.

[Exit]

[Enter HOTSPUR]

Hotspur (Henry Percy). If I mistake not, thou art Harry Monmouth.

Henry V. Thou speak'st as if I would deny my name.

Hotspur (Henry Percy). My name is Harry Percy.

Henry V. Why, then I see 
A very valiant rebel of the name.
I am the Prince of Wales; and think not, Percy, 
To share with me in glory any more: 
Two stars keep not their motion in one sphere; 
Nor can one England brook a double reign, 
Of Harry Percy and the Prince of Wales.

Hotspur (Henry Percy). Nor shall it, Harry; for the hour is come 
To end the one of us; and would to God 
Thy name in arms were now as great as mine!

Henry V. I'll make it greater ere I part from thee; 
And all the budding honours on thy crest
I'll crop, to make a garland for my head.

Hotspur (Henry Percy). I can no longer brook thy vanities.

[They fight]

[Enter FALSTAFF]

Falstaff. Well said, Hal! to it Hal! Nay, you shall find no
boy's play here, I can tell you. 
[Re-enter DOUGLAS; he fights with FALSTAFF,] 
who falls down as if he were dead, and exit 
DOUGLAS. HOTSPUR is wounded, and falls]

Hotspur (Henry Percy). O, Harry, thou hast robb'd me of my youth!
I better brook the loss of brittle life 
Than those proud titles thou hast won of me; 
They wound my thoughts worse than sword my flesh: 
But thought's the slave of life, and life time's fool; 
And time, that takes survey of all the world,
Must have a stop. O, I could prophesy, 
But that the earthy and cold hand of death 
Lies on my tongue: no, Percy, thou art dust 
And food for—

[Dies]

Henry V. For worms, brave Percy: fare thee well, great heart! 
Ill-weaved ambition, how much art thou shrunk! 
When that this body did contain a spirit, 
A kingdom for it was too small a bound; 
But now two paces of the vilest earth
Is room enough: this earth that bears thee dead 
Bears not alive so stout a gentleman. 
If thou wert sensible of courtesy, 
I should not make so dear a show of zeal: 
But let my favours hide thy mangled face;
And, even in thy behalf, I'll thank myself 
For doing these fair rites of tenderness. 
Adieu, and take thy praise with thee to heaven! 
Thy ignominy sleep with thee in the grave, 
But not remember'd in thy epitaph!
[He spieth FALSTAFF on the ground] 
What, old acquaintance! could not all this flesh 
Keep in a little life? Poor Jack, farewell! 
I could have better spared a better man: 
O, I should have a heavy miss of thee,
If I were much in love with vanity! 
Death hath not struck so fat a deer to-day, 
Though many dearer, in this bloody fray. 
Embowell'd will I see thee by and by: 
Till then in blood by noble Percy lie.

[Exit PRINCE HENRY]

Falstaff. [Rising up] Embowelled! if thou embowel me to-day, 
I'll give you leave to powder me and eat me too 
to-morrow. 'Sblood,'twas time to counterfeit, or 
that hot termagant Scot had paid me scot and lot too.
Counterfeit? I lie, I am no counterfeit: to die, 
is to be a counterfeit; for he is but the 
counterfeit of a man who hath not the life of a man: 
but to counterfeit dying, when a man thereby 
liveth, is to be no counterfeit, but the true and
perfect image of life indeed. The better part of 
valour is discretion; in the which better part I 
have saved my life.'Zounds, I am afraid of this 
gunpowder Percy, though he be dead: how, if he 
should counterfeit too and rise? by my faith, I am
afraid he would prove the better counterfeit. 
Therefore I'll make him sure; yea, and I'll swear I 
killed him. Why may not he rise as well as I? 
Nothing confutes me but eyes, and nobody sees me. 
Therefore, sirrah,
[Stabbing him] 
with a new wound in your thigh, come you along with me.

[Takes up HOTSPUR on his back]

[Re-enter PRINCE HENRY and LORD JOHN OF LANCASTER]

Henry V. Come, brother John; full bravely hast thou flesh'd
Thy maiden sword.

Prince John. But, soft! whom have we here? 
Did you not tell me this fat man was dead?

Henry V. I did; I saw him dead, 
Breathless and bleeding on the ground. Art 
thou alive? 
Or is it fantasy that plays upon our eyesight? 
I prithee, speak; we will not trust our eyes 
Without our ears: thou art not what thou seem'st.

Falstaff. No, that's certain; I am not a double man: but if I
be not Jack Falstaff, then am I a Jack. There is Percy: 
[Throwing the body down] 
if your father will do me any honour, so; if not, let 
him kill the next Percy himself. I look to be either 
earl or duke, I can assure you.

Henry V. Why, Percy I killed myself and saw thee dead.

Falstaff. Didst thou? Lord, Lord, how this world is given to 
lying! I grant you I was down and out of breath; 
and so was he: but we rose both at an instant and 
fought a long hour by Shrewsbury clock. If I may be
believed, so; if not, let them that should reward 
valour bear the sin upon their own heads. I'll take 
it upon my death, I gave him this wound in the 
thigh: if the man were alive and would deny it, 
'zounds, I would make him eat a piece of my sword.

Prince John. This is the strangest tale that ever I heard.

Henry V. This is the strangest fellow, brother John. 
Come, bring your luggage nobly on your back: 
For my part, if a lie may do thee grace, 
I'll gild it with the happiest terms I have.
[A retreat is sounded] 
The trumpet sounds retreat; the day is ours. 
Come, brother, let us to the highest of the field, 
To see what friends are living, who are dead.

[Exeunt PRINCE HENRY and LANCASTER]

Falstaff. I'll follow, as they say, for reward. He that 
rewards me, God reward him! If I do grow great, 
I'll grow less; for I'll purge, and leave sack, and 
live cleanly as a nobleman should do.

[Exit]
", 'words' => ' [alarum excursions enter prince henry lord john lancaster earl westmoreland] iv prithee harry withdraw thyself thou bleed st unless beseech majesty lest retirement amaze friends westmoreland lead tent ll help god forbid shallow scratch drive wales field stain nobility lies trodden rebels arms triumph massacres breathe cousin duty sake [exeunt hast deceived thee spirit loved brother respect soul hold percy lustier maintenance look ungrown warrior boy lends mettle [exit] [enter douglas] douglas king grow hydra heads fatal wear colours art counterfeit person grieves heart shadows met boys seek seeing fall luckily assay defend fear faith bear mine whoe er win [they fight danger enters] thy head vile scot spirits valiant shirley stafford blunt threatens promiseth means pay flies] cheerly fares grace sir nicholas gawsey hath succor sent clifton straight stay awhile redeem lost opinion makest tender life fair rescue brought injury hearken death insulting hand speedy poisonous potions world saved treacherous labour son hotspur] hotspur (henry percy) mistake monmouth speak deny name rebel share glory stars motion sphere nor england brook double reign hour ere budding honours crest crop garland vanities fight] falstaff] falstaff hal nay play tell [re-enter fights ] falls dead exit wounded falls] robb youth loss brittle proud titles won wound worse sword flesh slave time fool takes survey stop prophesy earthy cold tongue dust food for— [dies] worms brave fare ill-weaved ambition shrunk body contain kingdom bound paces vilest earth bears alive stout gentleman wert sensible courtesy dear zeal favours hide mangled behalf thank doing rites tenderness adieu praise heaven ignominy sleep grave remember epitaph [he spieth ground] acquaintance little poor jack farewell spared heavy miss love vanity struck fat deer to-day dearer bloody fray embowell till blood noble lie [exit henry] [rising up] embowelled embowel leave powder eat to-morrow sblood twas hot termagant paid lot die dying thereby liveth true perfect image indeed valour discretion zounds afraid gunpowder rise prove yea swear killed confutes eyes sirrah [stabbing him] thigh [takes back] lancaster] bravely maiden soft whom breathless bleeding ground fantasy plays eyesight trust ears [throwing down] father honour kill duke assure didst lying grant breath rose instant fought shrewsbury clock believed reward sin own piece strangest tale heard fellow bring luggage nobly gild happiest terms [a retreat sounded] trumpet sounds day ours living follow rewards purge sack live cleanly nobleman ']);



	###################### SCENE 5 ########################
	Document::create([
'content' => " [The trumpets sound. Enter KING HENRY IV, PRINCE HENRY, LORD JOHN LANCASTER, EARL OF WESTMORELAND, with WORCESTER and VERNON prisoners]

Henry IV. Thus ever did rebellion find rebuke. 
Ill-spirited Worcester! did not we send grace, 
Pardon and terms of love to all of you? 
And wouldst thou turn our offers contrary?
Misuse the tenor of thy kinsman's trust? 
Three knights upon our party slain to-day, 
A noble earl and many a creature else 
Had been alive this hour, 
If like a Christian thou hadst truly borne 
Betwixt our armies true intelligence.

Earl of Worcester. What I have done my safety urged me to; 
And I embrace this fortune patiently, 
Since not to be avoided it falls on me.

Henry IV. Bear Worcester to the death and Vernon too:
Other offenders we will pause upon. 
[Exeunt WORCESTER and VERNON, guarded] 
How goes the field?

Henry V. The noble Scot, Lord Douglas, when he saw 
The fortune of the day quite turn'd from him,
The noble Percy slain, and all his men 
Upon the foot of fear, fled with the rest; 
And falling from a hill, he was so bruised 
That the pursuers took him. At my tent 
The Douglas is; and I beseech your grace
I may dispose of him.

Henry IV. With all my heart.

Henry V. Then, brother John of Lancaster, to you 
This honourable bounty shall belong: 
Go to the Douglas, and deliver him
Up to his pleasure, ransomless and free: 
His valour shown upon our crests to-day 
Hath taught us how to cherish such high deeds 
Even in the bosom of our adversaries.
Prince John. I thank your grace for this high courtesy,
Which I shall give away immediately.

Henry IV. Then this remains, that we divide our power. 
You, son John, and my cousin Westmoreland 
Towards York shall bend you with your dearest speed, 
To meet Northumberland and the prelate Scroop,
Who, as we hear, are busily in arms: 
Myself and you, son Harry, will towards Wales, 
To fight with Glendower and the Earl of March. 
Rebellion in this land shall lose his sway, 
Meeting the cheque of such another day:
And since this business so fair is done, 
Let us not leave till all our own be won.

[Exeunt]
", 'words' => ' [the trumpets sound enter king henry iv prince lord john lancaster earl westmoreland worcester vernon prisoners] rebellion rebuke ill-spirited send grace pardon terms love wouldst thou offers contrary misuse tenor thy kinsman trust knights party slain to-day noble creature else alive hour christian hadst truly borne betwixt armies true intelligence safety urged embrace fortune patiently avoided falls bear death offenders pause [exeunt guarded] goes field scot douglas day percy foot fear fled rest falling hill bruised pursuers tent beseech dispose heart brother honourable bounty belong deliver pleasure ransomless free valour shown crests hath taught cherish deeds bosom adversaries thank courtesy immediately remains divide power son cousin towards york bend dearest speed meet northumberland prelate scroop hear busily arms harry wales fight glendower march land lose sway meeting cheque business fair leave till own won [exeunt] ']);

    }
}
