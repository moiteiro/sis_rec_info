<?php 
function truncate($text, $size)
{
	return strlen($text) > $size ? substr($text, 0, $size)."..." : $text;
}