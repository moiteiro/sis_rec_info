<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
    	$input = request()->input();
    	
    	if (isset($input['search'])) {
	    	$words = preg_replace('/\s+/', ' ', $input['search']);
	    	$words = strtolower($words);
	    	$words = explode(" ",trim($words));
    	}

    	$documents = [];
    	if (isset($input['searchType'])) {
	    	if ($input['searchType'] == 1) {
	    		$documents = Document::binary_search($words);
	    	} else {
	    		$documents = Document::vectorial_search($words);
	    	}
    		$statistics['total'] = count($documents);
    	} else {
    		$input['search'] = "";
    		$input['searchType'] = "";
    	}

    	return view('search.index', compact('documents', 'input', 'statistics'));
    }
}
