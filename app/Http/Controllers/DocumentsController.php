<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;

class DocumentsController extends Controller
{
    public function index () {
    	$documents = Document::all();

    	return view('documents.index', compact('documents'));
    }

    public function show($id) {
    	$document = Document::find($id);
    	return view('documents.show', compact('document'));
    }

    function create()
    {
        return view('documents.create');
    }

    function store() 
    {
        $input = request()->input();
        
        $document = Document::create(['content' => $input['content']]);

        $document->compressContent();

        $document->save();

        return \Redirect::to('/documents');
    }

    function edit($id)
    {
    	$document = Document::find($id);
    	return view('documents.edit', compact('document'));
    }

    function update($id)
    {
        $input = request()->input();
        
        $document = Document::find($id);

        $document->content = $input['content'];
        $document->compressContent();

        $document->save();

        return \Redirect::to('/documents');
    }

    function destroy($id)
    {
        $document = Document::find($id);

        $document->delete();

        return \Redirect::to('/documents');
    }
}
