<?php

namespace App\Models;

use File;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = 
    [
        'scene',
    	'content',
        'words',
        'dictionary',
        'similarity',
    ];

    public function compressContent() 
    {
        $dic = $this->createDictionary();
        $words = array_keys($dic);
        unset($this->attributes['dictionary']);
        $this->attributes['words'] = " " . implode(" ", $words) . " ";
    }

    public function createDictionary() 
    {
        $stopwords = File::getRequire(app_path().'/stopwords.php');

        // removing break and new lines
        $content = preg_replace( "/\r|\n/", " ", $this->content);
        // removing puntations and some extra characters
        $content = preg_replace( "/\.|\,|\'|\:|\;|\?|\!|\]|\[|\(|\)|\"/", " ", $content);
        $words = explode(" ", $content);

        $dictionary = [];

        foreach($words as $word) {
            $word = strtolower($word);

            if ($word == "")
                continue;

            if (in_array($word, $stopwords))
                continue;

            if(!isset($dictionary[$word])) {
                $dictionary[$word] = 1;
            } else {
                $dictionary[$word]++;
            }
        }
        
        $this->dictionary = $dictionary;
        return $dictionary;
    }

    static public function binary_search($words, $logical_operator = "AND") 
    {
        $query = Document::whereRaw(1);
        $string = [];

        foreach ($words as $word) {
            $string[]= "LOWER(words) LIKE '% " . strtolower($word). " %'";
        }
        $string = implode(" $logical_operator ", $string);
        $query = $query->whereRaw($string);
        return $query->get();
    }

    static public function vectorial_search($terms)
    {
        $search = [];
        $words_counting = [];

        if (isset($terms[0])) {
            $search[$terms[0]] = 0.2;
            $words_counting[$terms[0]] = 0;
        }
        if (isset($terms[1])) {
            $search[$terms[1]] = 0.3;
            $words_counting[$terms[1]] = 0;
        }
        if (isset($terms[2])) {
            $search[$terms[2]] = 0.5;
            $words_counting[$terms[2]] = 0;
        }

        // getting all documents that contains the search terms.
        $documents = self::binary_search($terms, "OR");
        // getting document corpus value
        $corpus = count(Document::all());
        // creating dictionary
        foreach ($documents as $document) {
            $document->createDictionary();
        }

        // getting to numbers of documents that contains the same words as the search
        foreach($terms as $term) {
            foreach ($documents as $document) {
                if (isset($document->dictionary[$term])) {
                    $words_counting[$term]++;
                }
            }
        }

        // calculating TF-IDF weighting and similiarity        
        foreach ($documents as $document) {
            $document->_calculateTFIDFWeighting($terms, $corpus, $words_counting);
            $document->_calculateSimilarity($search);
        }

        // ordering by similarity.
        $documents = $documents->sortByDesc('similarity');
        
        return $documents;
    }

    protected function _calculateSimilarity($search) 
    {
        $this->attributes['similarity'] = $this->_similarity($search, $this->dictionary);
    }

    protected function _calculateTFIDFWeighting($terms, $corpus, $words_counting)
    {
        $max_freq = max($this->dictionary);

        foreach($terms as $term) {
            if (isset($this->dictionary[$term])) {
                $tf = $this->dictionary[$term] / $max_freq;
                $idf = $corpus / $words_counting[$term];
                $idf = log($idf, 10);
                $this->attributes['dictionary'][$term] = $tf * $idf;
            }
        }
    }

    protected function _similarity(array $search, array $dictionary)
    {
        $dotProduct = $this->_dot_product($search, $dictionary);
        $absVector1 = $this->_abs_vector($search);
        $absVector2 = $this->_abs_vector($dictionary);

        return  $dotProduct / ( $absVector1 * $absVector2);
    }

    protected function _dot_product(array $search, array $dictionary)
    {
        $result = 0;

        foreach ($search as $term => $occorencies1) {
            foreach ($dictionary as $word2 => $occorencies2) {
                if ($term === $word2) 
                    $result += $search[$term] * $dictionary[$word2];
            }
        }

        return $result;
    }

    protected function _abs_vector(array $vec)
    {
        $result = 0;

        foreach (array_values($vec) as $value) {
            $result += $value * $value;
        }

        return sqrt($result);
    }
}
