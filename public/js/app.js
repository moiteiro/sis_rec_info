$().ready(function(){
	$('.remove-entry-button').click(function() {

	    var name = $(this).attr('data-entry-name');
	    var id = $(this).attr('data-entry-id');

	    $('#target_form_delete').html(id + ' - <strong>' + name + '</strong>');
	    $('#remove_entry_confirmation_form').attr('action', $(this).attr('data-remove-url'));
	});

	function readFile(evt) {
		var f = evt.target.files[0]; 

		if (f) {
			var r = new FileReader();
			r.onload = function(e) {
				var contents = e.target.result;
				$('#content').val(contents);
			}
			r.readAsText(f);
		}
	}

	$('#file').change(function(event){
		readFile(event);
	});

});